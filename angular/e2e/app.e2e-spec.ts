import { MOOClyserTemplatePage } from './app.po';

describe('MOOClyser App', function() {
  let page: MOOClyserTemplatePage;

  beforeEach(() => {
    page = new MOOClyserTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
