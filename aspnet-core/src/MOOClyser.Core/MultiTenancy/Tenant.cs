﻿using Abp.MultiTenancy;
using MOOClyser.Authorization.Users;

namespace MOOClyser.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
