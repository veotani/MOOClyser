﻿namespace MOOClyser
{
    public class MOOClyserConsts
    {
        public const string LocalizationSourceName = "MOOClyser";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
