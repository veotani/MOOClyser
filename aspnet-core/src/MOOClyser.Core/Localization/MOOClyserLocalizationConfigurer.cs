﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace MOOClyser.Localization
{
    public static class MOOClyserLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(MOOClyserConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(MOOClyserLocalizationConfigurer).GetAssembly(),
                        "MOOClyser.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
