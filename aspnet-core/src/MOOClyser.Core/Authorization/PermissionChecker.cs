﻿using Abp.Authorization;
using MOOClyser.Authorization.Roles;
using MOOClyser.Authorization.Users;

namespace MOOClyser.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
