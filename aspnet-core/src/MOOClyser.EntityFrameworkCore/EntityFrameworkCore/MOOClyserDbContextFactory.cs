﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MOOClyser.Configuration;
using MOOClyser.Web;

namespace MOOClyser.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class MOOClyserDbContextFactory : IDesignTimeDbContextFactory<MOOClyserDbContext>
    {
        public MOOClyserDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MOOClyserDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            MOOClyserDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MOOClyserConsts.ConnectionStringName));

            return new MOOClyserDbContext(builder.Options);
        }
    }
}
