﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using MOOClyser.Authorization.Roles;
using MOOClyser.Authorization.Users;
using MOOClyser.MultiTenancy;

namespace MOOClyser.EntityFrameworkCore
{
    public class MOOClyserDbContext : AbpZeroDbContext<Tenant, Role, User, MOOClyserDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public MOOClyserDbContext(DbContextOptions<MOOClyserDbContext> options)
            : base(options)
        {
        }
    }
}
