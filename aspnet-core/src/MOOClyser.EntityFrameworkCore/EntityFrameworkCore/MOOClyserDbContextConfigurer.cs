using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace MOOClyser.EntityFrameworkCore
{
    public static class MOOClyserDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<MOOClyserDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<MOOClyserDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
