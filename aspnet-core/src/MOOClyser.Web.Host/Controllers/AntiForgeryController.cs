using Microsoft.AspNetCore.Antiforgery;
using MOOClyser.Controllers;

namespace MOOClyser.Web.Host.Controllers
{
    public class AntiForgeryController : MOOClyserControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
