﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MOOClyser.Configuration;

namespace MOOClyser.Web.Host.Startup
{
    [DependsOn(
       typeof(MOOClyserWebCoreModule))]
    public class MOOClyserWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MOOClyserWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MOOClyserWebHostModule).GetAssembly());
        }
    }
}
