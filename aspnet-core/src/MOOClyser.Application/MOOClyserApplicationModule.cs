﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MOOClyser.Authorization;

namespace MOOClyser
{
    [DependsOn(
        typeof(MOOClyserCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class MOOClyserApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<MOOClyserAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(MOOClyserApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
