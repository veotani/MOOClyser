﻿using System.Threading.Tasks;
using MOOClyser.Configuration.Dto;

namespace MOOClyser.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
