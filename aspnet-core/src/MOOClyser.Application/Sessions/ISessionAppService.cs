﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MOOClyser.Sessions.Dto;

namespace MOOClyser.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
