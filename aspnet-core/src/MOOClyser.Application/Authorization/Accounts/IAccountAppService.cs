﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MOOClyser.Authorization.Accounts.Dto;

namespace MOOClyser.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
