﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MOOClyser.MultiTenancy.Dto;

namespace MOOClyser.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

