using System.ComponentModel.DataAnnotations;

namespace MOOClyser.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}