using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace MOOClyser.Controllers
{
    public abstract class MOOClyserControllerBase: AbpController
    {
        protected MOOClyserControllerBase()
        {
            LocalizationSourceName = MOOClyserConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
