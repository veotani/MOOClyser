﻿using Abp.AutoMapper;
using MOOClyser.Authentication.External;

namespace MOOClyser.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
